import java.util.ArrayList;

public class Node<T> {

    private T value;
    private ArrayList<T> nodeEdges;

    public Node(T value) {
        this.value = value;
        this.nodeEdges = new ArrayList<T>();
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public ArrayList<T> getNodeEdges() {
        return nodeEdges;
    }

    public void setNodeEdges(ArrayList<T> nodeEdges) {
        this.nodeEdges = nodeEdges;
    }

    @Override
    public String toString() {
        return "key: "+ value;
    }

}
