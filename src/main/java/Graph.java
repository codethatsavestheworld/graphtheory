import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    private static final Logger LOG = Logger.getLogger(Graph.class);
    private HashMap<Node, ArrayList> graphData;

    public Graph() {}

    public Graph(int[][] adjacencyMatrix) {

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            Node<Integer> node = new Node<Integer>(i + 1);

            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] == 1) {
                    node.getNodeEdges().add(j);
                }
            }
            LOG.info("node:" + node.getValue() + " connections:" + node.getNodeEdges());
        }
    }

    public void depthFirstSearch(Map<String, Integer> graph) {
        graph.entrySet();
    }

    public HashMap<Node, ArrayList> getGraphData() {
        return graphData;
    }

    public void setGraphData(HashMap<Node, ArrayList> graphData) {
        this.graphData = graphData;
    }

}
